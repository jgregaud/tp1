#ifndef TOTOTOTOTO2
#define TOTOTOTOTO2
typedef struct image111{

BITMAP* matrice[IMG] ;
BITMAP* perso ;
BITMAP* objet[3];
BITMAP* fin ;
BITMAP* mechant;

}t_sprite ;

void lancer_allegro();
void afficher_matrice(t_sprite* image, t_matrice* carte,BITMAP* page );
t_sprite* charger_images();
BITMAP* imgload(char* name);
void afficher_perso(t_sprite* image, t_perso* perso,BITMAP* page,int ident , int level);
void afficher_objet(t_sprite* image, t_objet* objet ,BITMAP* page);
void animation_fin(BITMAP* page,t_sprite* image);
void afficher_mechant(t_sprite* image,t_ennemis* mechant,BITMAP* page);



#endif //TOTOTOTOTO2
