#ifdef __cplusplus
#error Be sure you are using a C compiler...
#endif

#if defined (WIN32) || defined (_WIN32)

#include <winsock2.h>

#elif defined (linux) || defined (_POSIX_VERSION) || defined (_POSIX2_C_VERSION)\
 || defined (_XOPEN_VERSION)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>             /* close */

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#define closesocket(s) close (s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

#else
#error not defined for this platform
#endif

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define RECEVOIR_CARTE 1111
#define GET_POS 1011
#define GET_IDENT 1001
#define UNGET_IDENT 0001

/* macros ============================================================== */

#define TELNET 35

/* we want to listen to the TELNET port */
#define PORT TELNET

#define ESC 27
int identification();
int maj_carte();
int desidentification(int ident);
int maj_coord(int identif,int x,int y,  int* x2, int *y2,int level, int* level2);
